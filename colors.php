<?php

// Bash colors
const RED = "\e[31m";
const GREEN = "\e[32m";
const YELLOW = "\e[33m";
const BLUE = "\e[34m";
const MAGENTA = "\e[35m";
const CYAN = "\e[36m";

// Bash backgrounds
const BGWHITE = "\e[7m";
const BGRED = "\e[41m";
const BGGREEN = "\e[42m";
const BGYELLOW = "\e[43m";
const BGBLUE = "\e[44m";
const BGMAGENTA = "\e[45m";

// Bash original color
const RESET = "\e[0m";