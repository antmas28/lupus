<?php

// Help

echo "\n";

echo RED;
echo "REGOLE UFFICIALI\n";
echo RESET;

sleep(1);
echo "\n";

echo YELLOW;
echo "SCOPO DEL GIOCO\n";
echo RESET;
echo "Nel gioco ci sono due fazioni: quella dei Lupi Mannari e quella degli Umani. Lo scopo dei Lupi Mannari è di eliminare tutti gli umani. Viceversa quello degli umani è di linciare tutti i Lupi Mannari.\n";

readline();

echo YELLOW;
echo "PREPARAZIONE\n";
echo RESET;
echo "Scegliete il moderatore: egli non appartiene a nessuna fazione e gestisce solo la partita. Gli altri giocatori interpretano il ruolo dato dalle carte. 
Separate le carte: i fantasmi, gli indizi, i personaggi. Componete un mazzo di personaggi a seconda del numero di giocatori come di seguito:
- con 8 giocatori (escluso il moderatore) usate: 5 villici, 2 lupi mannari e il veggente;
- con 9 o più giocatori, aggiungete carte a suffi cienza scegliendo altri villici e/o personaggi
speciali, indicati alla fi ne del regolamento;
- con 16 o più giocatori, aggiungete un terzo lupo mannaro.
Il moderatore mischia i personaggi e distribuisce una carta personaggio coperta a ogni giocatore, guardandola prima di consegnarla e memorizzando se il giocatore che la riceve è un lupo mannaro.
I giocatori guardano segretamente la propria carta e la tengono coperta fi no alla fi ne del gioco. Ogni giocatore riceve un gettone voto. Il moderatore prende la carta riassuntiva e tiene a portata di mano i fantasmi, i due indizi e il “Benvenuto!”.\n";

readline();

echo RED;
echo "IL GIOCO\n";
echo RESET;
echo "Il gioco si divide in due fasi: la notte e il giorno. Di notte i lupi mannari sbranano un giocatore. Di giorno si discute e si lincia qualcuno, sperando che sia un lupo mannaro. Tra gli umani si cela un veggente che di notte è in grado di sondare la mente di un giocatore, e capire se si tratta di un lupo mannaro o meno.\n";

readline();

echo BLUE;
echo "La Notte\n";
echo RESET;
echo "Il moderatore dichiara l’inizio della notte (“è notte, chiudete tutti gli occhi”). Tutti i giocatori chiudono gli occhi. Per coprire eventuali altri rumori, di notte tutti i giocatori battono ritmicamente una mano sul tavolo. Il moderatore chiama ad alta voce il veggente (per es. “il veggente apre gli occhi e sceglie una persona”).
Il veggente apre gli occhi e indica con un gesto un giocatore a sua scelta. Il moderatore gli risponde con un cenno del capo affermativo se il giocatore indicato è un lupo mannaro, negativo altrimenti. Il moderatore quindi chiama la fi ne della fase del veggente (“il veggente chiude gli occhi”), il quale richiude gli occhi.
Nota: questa fase va giocata anche se il veggente è un fantasma, per non dare indizi agli altri; in questo caso il moderatore chiama il veggente e, poco dopo, la fi ne della fase senza che il veggente gli risponda. Il moderatore quindi chiama i lupi mannari (“i lupi mannari aprono gli occhi e scelgono una persona da sbranare”). I lupi mannari aprono gli occhi e si riconoscono. Dopodiché si mettono rapidamente e silenziosamente d’accordo su quale giocatore sbranare, indicandolo con un cenno. Il moderatore prende atto della decisione e chiama la fi ne della fase dei lupi mannari (“i lupi mannari chiudono gli occhi”), i quali richiudono gli occhi. A questo punto la notte è terminata.\n";

readline();

echo CYAN;
echo "Il Giorno\n";
echo RESET;
echo "Il moderatore dichiara ora l’inizio del giorno (“è giorno, aprite tutti gli occhi”). Tutti i giocatori aprono gli occhi. Il moderatore dice al giocatore scelto dai Lupi Mannari che è stato sbranato! Questo giocatore da ora in poi non gioca più come personaggio ma come Fantasma! Egli deve astenersi da commenti e non può più parlare per il resto della partita. Egli riceve una carta fantasma, e il “Benvenuto!”. (Ricordate che non può rivelare il proprio personaggio!).
I giocatori devono ora scegliere qualcuno da linciare; possono parlare fra loro per identifi care chi siano i lupi mannari attraverso ipotesi, deduzioni e contraddizioni. I lupi mannari, mescolati tra i superstiti, tenderanno a convogliare i sospetti verso altri villici o verso il giocatore che reputano il veggente. Si può mentire liberamente, ma non si può mai mostrare la propria carta agli altri!

Dopo un massimo di tre minuti di discussione, il moderatore chiede a ogni giocatore, partendo da chi è a sinistra di quello che ha il “Benvenuto!” e procedendo in senso orario, chi secondo lui vada linciato.
Tutti i giocatori, inclusi i fantasmi, prendono in mano il gettone voto, e al proprio turno votano mettendo il gettone davanti al giocatore che vogliono linciare (in questa fase, il lato su cui viene giocato l’indizio non ha importanza). I due giocatori con più voti (cioè con più gettoni davanti) sono indiziati. Eventuali casi di
parità vengono risolti scegliendo chi è più vicino al giocatore col “Benvenuto!”, in senso orario. Ogni giocatore riprende il proprio gettone voto. Il moderatore mette davanti ai due indiziati un indizio di licantropia a testa: uno riceve l’impronta, l’altro il ciuffo di peli. Esempio. Durante il primo giorno, vi sono 8 giocatori vivi e un fantasma (9 voti in tutto). Il
giro di votazioni è terminato: Andrea ha preso 3 voti, Daniela e Roberta 2, Bruno e Carla 1 ciascuno. Andrea è indiziato. Daniela è più vicina di Roberta, in senso orario, a Francesco, che ha il “Benvenuto!”. Pertanto è Daniela a essere l’altra indiziata. Andrea e Daniela ricevono un indizio a testa.

I due giocatori indiziati possono difendersi con un ultimo breve discorso. Terminate le arringhe, i giocatori non indiziati e ancora vivi (esclusi quindi gli indiziati e i fantasmi) votano di nuovo il giocatore tra gli indiziati che verrà linciato. Questa votazione avviene in segreto e in contemporanea.
I giocatori posano sul tavolo il gettone voto col lato che corrisponde all’indizio di licantropia del giocatore che vogliono linciare verso l’alto, e lo coprono con
la mano. Quando tutti hanno scelto, i voti vengono rivelati allo stesso tempo, al comando del moderatore. Chi ha preso più voti è linciato e diventa un fantasma!
In caso di parità viene linciato il più vicino in senso orario a chi ha il “Benvenuto!”. Il moderatore recupera gli indizi, e dà al giocatore linciato una carta fantasma. Il giocatore linciato deve
astenersi dal commentare la decisione, e non può più parlare fi no alla fi ne della partita (né rivelare il proprio personaggio!).

A questo punto il giorno è terminato: si ricomincia con una nuova notte e così via, fi nché una fazione non vince. Esempio.
Andrea e Daniela, indiziati di licantropia, si difendono con un’arringa per provare che non sono dei lupi mannari: saranno stati convincenti? Il moderatore ora annuncia: “Andrea è indiziato per dei ciuffi di pelo di lupo mannaro trovati in casa sua, Daniela è indiziata per un’orma di lupo mannaro reperita nel giardino. Scegliete chi linciare!” Tutti i giocatori ancora vivi (tranne i fantasmi e gli indiziati Andrea e Daniela) scelgono chi linciare, segretamente. Il moderatore annuncia “Al mio via, rivelate i vostri voti. 1, 2, 3... via!”. I voti vengono rivelati. Due
mostrano il ciuffo di peli di lupo mannaro per Andrea, mentre quattro mostrano l’orma di lupo mannaro per Daniela. Povera Daniela, i villici inferociti l’hanno linciata credendola un lupo mannaro: avranno fatto bene?\n";

readline();

echo RED;
echo "FINE DELLA PARTITA\n";
echo RESET;
echo "Il moderatore dichiara la partita fi nita con una vittoria degli umani se questi linciano tutti i lupi mannari.
I lupi mannari sono invece dichiarati vincitori se in un qualunque momento sono in numero pari agli umani ancora vivi (per esempio, 2 lupi mannari e 2 umani, oppure 1 e 1): in tal caso i lupi mannari sbranano gli umani rimasti senza tanti complimenti!
È sempre l’intera fazione a vincere: quindi anche i fantasmi partecipano alla vittoria!\n";

readline();

echo RED;
echo "SUGGERIMENTI STRATEGICI\n";
echo RESET;
echo "Gli umani devono cercare di capire chi siano i lupi mannari. I lupi mannari hanno interesse a spacciarsi per umani e a gettare discredito sugli umani veri. Il veggente può cercare di indirizzare i sospetti sui lupi mannari che scopre, ma senza rivelarsi troppo, altrimenti i lupi mannari ne faranno
un sol boccone! Naturalmente il veggente può mettersi in evidenza se pensa che ne valga la pena.
Attenti però: anche un lupo mannaro potrebbe spacciarsi per il veggente! Diffi date di giocatori che concordano troppo tra loro e ricordate: accusare qualcuno di essere un lupo mannaro è sospetto, ma non accusarlo può esserlo ancora di più!\n";

readline();

echo RED;
echo "SUGGERIMENTI PRATICI\n";
echo RESET;
echo "• Durante la prima notte, per evitare che i lupi mannari scelgano a caso, e per dare a tutti i giocatori la possibilità di giocare una giornata intera, è buona prassi che sia sbranato il moderatore. La notte i lupi mannari apriranno solo gli occhi per riconoscersi, e sceglieranno implicitamente il moderatore
come vittima. Durante il primo giorno il moderatore dunque annuncerà che egli stesso è stato sbranato. I giocatori nello scegliere qualcuno da linciare si potranno basare a questo punto sulle informazioni del veggente – e su quelle dei lupi mannari...
• Battere ritmicamente una mano sul tavolo serve a coprire eventuali rumori che inavvertitamente possono essere causati dai giocatori, durante la notte. Usate solo una mano, e tenete l’altra mano libera per potere eventualmente indicare altri giocatori durante la fase notturna.
• È buona norma per il moderatore porsi al centro del gruppo e parlare, di notte, sempre verso il centro del tavolo, e non in direzione dei giocatori, per non dare involontariamente indizi. Inoltre il moderatore dovrebbe evitare di usare pronomi come “lui” o “lei” e simili (ad es. “la veggente” fa
capire che si tratta di una donna), e restare il più generico possibile.
• Se il gruppo di gioco è molto vasto, o non state giocando attorno a un tavolo, c’è la possibilità che sia scomodo votare mettendo il proprio gettone davanti a un altro. In questo caso, durante la votazione per scegliere i giocatori indiziati, invece di usare i gettoni, i giocatori votano semplicemente indicando uno degli altri. Man mano che i giocatori ottengono voti, li segnano usando le dita della mano, tenendole ben visibili a tutti. I fantasmi restituiscono il gettone voto; la votazione per il linciaggio avviene come di consueto. Gli indiziati non votano, e per renderlo ancora più chiaro a tutti, coprono il proprio gettone con gli indizi.\n";

readline();
echo str_repeat("-", 158) . "\n\n";

echo YELLOW;
echo "PERSONAGGI E POTERI\n";
echo RESET;
echo "\n";

echo RED;
echo "LUPI MANNARI\n";
echo RESET;
echo "Sono gli unici a riconoscersi durante la notte durante la quale scelgono un giocatore da uccidere.\n";

readline();

echo GREEN;
echo "VILLICI\n";
echo RESET;
echo "Non hanno nessun potere e devono confrontarsi durante il giorno per eliminare i Lupi.\n";

readline();

echo BGWHITE;
echo "FANTASMI\n";
echo RESET;
echo "Tutti coloro che muoiono perdono i loro poteri e possono continuare a giocare rimanendo in silenzio e non rivelando il proprio ruolo. Possono partecipare alla votazione per il ballottaggio, ma non all'eliminazione diretta. Sono fondamentali per chiudere la partita.\n";

readline();

echo CYAN;
echo "VEGGENTE\n";
echo RESET;
echo "Durante la notte esamina un giocatore tra i vivi solo per sapere se è o non è un Lupo.\n";

readline();

echo CYAN;
echo "MEDIUM\n";
echo RESET;
echo "Durante la notte esamina un giocatore tra i morti solo per sapere se è o non è un Lupo.\n";

readline();

echo BLUE;
echo "INDEMONIATO\n";
echo RESET;
echo "E' visto come buono da Veggente e Medium ma parteggia per i lupi, senza conoscerli. Deve impegnarsi a capire l’identità dei Lupi e a spostare l’attenzione sui buoni. Pertanto vince se vincono i Lupi. Consiglio: in casi estremi, fingersi Lupo e sacrificarsi per salvare un vero Lupo.\n";

readline();

echo CYAN;
echo "GUARDIA DEL CORPO\n";
echo RESET;
echo "Durante la notte sceglie un giocatore da proteggere dai lupi e dal Gufo Letale (20+). Non può proteggere sè stessa.\n";

readline();

echo CYAN;
echo "GUFO\n";
echo RESET;
echo "Durante la notte sceglie un giocatore da mandare direttamente al ballottaggio. Da 20+ giocatori diventa Gufo Letale, comunque buono, e avrà il potere di uccidere direttamente il giocatore scelto. Pertanto la mattina seguente ci possono essere potenzialmente due morti (Lupi + Gufo Letale).\n";

readline();

echo CYAN;
echo "MASSONI\n";
echo RESET;
echo "Giocano sempre in coppia e sono sostanzialmente villici. Il loro unico potere è quello di riconoscersi tra loro durante la prima notte in modo da sapere fin da subito che almeno loro sono buoni.\n";

readline();

echo MAGENTA;
echo "CRICETO MANNARO\n";
echo RESET;
echo "E' un giocatore solitario che viene visto come buono e muore se esaminato dal Veggente durante la notte. Vince la partita se riesce a rimanere in vita fino alla fine. Essendo un mannaro non puo' essere ucciso dai Lupi, pertanto se i Lupi indicassero il criceto, non morirebbe nessuno.\n";

readline();

echo CYAN;
echo "MITO" . RED . "MANE\n";
echo RESET;
echo "Durante la prima notte sceglie un giocatore da cui assorbire il ruolo e con cui concordare le scelte. Se il giocatore scelto non è il Veggente o un Lupo, il mitomane diventerà un semplice villico.\n
VARIANTE MITOMANE 2.0 - Entro le prime tre notti deve scegliere un giocatore da cui assorbire il ruolo e con cui concordare le scelte. Se il giocatore scelto non è il Veggente o un Lupo, il mitomane diventerà un semplice villico. Muore se indica il Criceto Mannaro.\n";

readline();

echo CYAN;
echo "BARON SAMEDI - DOTTORE\n";
echo RESET;
echo "Puo' resuscitare una persona a partita a sua scelta. A partire dalla seconda notte potra' scegliere e verra' chiamato dal moderatore ogni notte finche' non utilizza il suo potere. Puo' utilizzare il suo potere anche se muore.\n";

readline();

echo CYAN;
echo "DORIAN GRAY\n";
echo RESET;
echo "Durante la prima notte indichera' al moderatore una persona a sua scelta alla quale affidare il suo ritratto. Se muore durante la partita, sia di giorno che di notte, puo' rivelare la sua carta e far morire la persona a cui ha affidato il suo ritratto, altrimenti muore. Esempio: Sono Dorian Gray e vengo ucciso. Il moderatore annuncia la mia morte. Se durante la partita mi rendo conto di aver affidato il mio ritratto ad una persona importante come il Veggente, non mi rivelo, sacrificandomi.\n";

echo str_repeat("-", 158) . "\n\n";