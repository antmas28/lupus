<?php

require('colors.php');

function loader($subject, $counter)
{
    for ($i = $counter; $i > 0 ; $i--)
    {
        $start = "$subject in $i";
        echo $start;
        sleep(1);
        echo "\r";
        echo str_repeat(" ", 50);
        echo "\r";
    }
}

// Creo un'array di ruoli da assegnare seguendo l'ordine di inserimento dei personaggi, come da regole ufficiali
$roles = [
    "Lupo Mannaro 1",      // 1
    "Lupo Mannaro 2",      // 2
    "Villico 1",           // 3
    "Villico 2",           // 4
    "Villico 3",           // 5
    "Villico 4",           // 6
    "Villico 5",           // 7
    "Veggente",            // 8 minimo di giocatori per poter iniziare
    "Medium",              // 9
    "Indemoniato",         // 10
    "Guardia del Corpo",   // 11
    "Gufo",                // 12
    "Massone 1",           // 13
    "Massone 2",           // 14
    "Criceto Mannaro",     // 15
    "Lupo Mannaro 3",      // 16
    "Mitomane",            // 17
    "Villico 6",           // 18
    "Villico 7",           // 19
    "Villico 8",           // 20
];

// Sezione per console S
// echo str_repeat("-", 84) . "\n";

// Sezione per console M
// echo str_repeat("-", 158) . "\n";

// Sezione per console L
// echo str_repeat("-", 229) . "\n";

// Sezione per console XL
// echo str_repeat("-", 238) . "\n";

echo RED; // Colore testo
include('logo.php');
echo RESET;

echo "\n";

sleep(1);

loader("Verifico i file, avvio", 5);

echo "\r";

echo "Last update 11/01/2021 16:17";

sleep(2);

echo "\r";

echo YELLOW; // Colore testo
echo "Benvenuto in Lupus Moderator v1.0\n";
echo RESET;

sleep(1);

echo "\n";

$help = readline("Conosci le regole del gioco, i personaggi e i loro poteri? (si/no):\n");

if ($help == "no") {
    
    include('help.php');
    
}

// In base al numero dei giocatori verranno selezionati in ordine i personaggi da assegnare (vedi array delle carte dei ruoli)
echo "\n";
echo GREEN;
echo "Iniziamo!\n";
echo RESET;
echo "\n";
$group = readline("Inserisci il numero di giocatori:\n");

// Numero minimo di giocatori richiesti 8, altrimenti il programma termina
if ($group >= 8 && $group <= count($roles)) 
{
    include('cards.php');
    
    include('game.php');
}
else
{
    echo str_repeat("-", 158) . "\n";
    echo RED;
    echo "Il numero di giocatori non soddisfa i requisiti (min 8 - max 20)\n";
    echo RESET;
}

// The End
sleep(1);
echo "\n";
echo GREEN;
echo str_repeat("-", 39) . "\n";
echo RESET;
echo "| Developed with <3 by Antonio Masoni |\n";
echo RED;
echo str_repeat("-", 39) . "\n";
echo RESET;
echo "\n";
