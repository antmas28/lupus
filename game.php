<?php

// TEST
// $group = 13;

// $chars = [
//     "Lupo Mannaro 1" => "Antonio",
//     "Lupo Mannaro 2" => "Andrea",
//     "Villico 1" => "Marco",
//     "Villico 2" => "Christian",
//     "Villico 3" => "Federica",
//     "Villico 4" => "Francesco",
//     "Villico 5" => "Gesu",
//     "Veggente" => "Giuseppe",
//     "Medium" => "Maria",
//     "Indemoniato" => "Bue",
//     "Guardia del Corpo" => "Asinello",
//     "Villico 6" => "Mario",
//     "Criceto Mannaro" => "Brazorf",
// ];

// echo "!! TEST !!\n";
// echo "\n";
// print_r($chars);
// echo "\n";

// Creo una funzione che dato il nome del giocatore votato, mi elimini l'intero elemento dall'array finale
function kill($array, $element)
{
    // Cerco il valore (nome del giocatore) e ottengo la chiave (ruolo)
    $searched = array_search($element, $array);

    // Tramite la chiave posso eliminare l'elemento
    unset($array[$searched]);

    return $array;
}

// Creo una funzione per cambiare il ruolo (chiave) del Mitomane in base alla sua scelta a inizio partita
function replaceRole($array, $oldRole, $newRole)
{
    // Ottengo una lista di tutte le chiavi
    $arrayKeys = array_keys($array);
    
    // Sostituisco la vecchia chiave con la nuova nell'array delle chiavi
    $oldRoleIndex = array_search($oldRole, $arrayKeys);
    $arrayKeys[$oldRoleIndex] = $newRole;

    // Ricombino le chiavi con l'array iniziale ottenendo un nuovo array ruolo => giocatore con la chiave aggiornata e l'ordine iniziale
    $array = array_combine($arrayKeys, $array);
    
    return $array;
}

loader("Avvio della partita", 3);

// Controllo la presenza del Mitomane, se si il Mitomane sceglie un giocatore di cui assumere le sembianze. Se il giocatore scelto e' un Lupo, il Mitomane diventa un Lupo, se Veggente, ci saranno 2 Veggenti che concorderanno chi esaminare ogni notte, se qualsiasi altra figura, diventa Villico
if ($group >= 17) 
{
    $myth = readline("Il Mitomane indichi un giocatore da cui assorbire i poteri:\n");

    $mythRole = array_search($myth, $chars);

        if ($mythRole === "Lupo Mannaro 1" || $mythRole === "Lupo Mannaro 2" || $mythRole === "Lupo Mannaro 3") {
            
            echo RED;
            echo $chars['Mitomane'] . " e' diventato Lupo Mannaro M.\n";
            echo RESET;

            $chars = replaceRole($chars, "Mitomane", "Lupo Mannaro M");

            sleep(2);

        }elseif ($mythRole == "Veggente") {
            
            echo CYAN;
            echo $chars["Mitomane"] . " e' diventato Veggente M.\n";
            echo RESET;
            
            $chars = replaceRole($chars, "Mitomane", "Veggente M");
    
            sleep(2);

        }else {
            
            echo YELLOW;
            echo $chars["Mitomane"] . " e' diventato Villico M.\n";
            echo RESET;

            $chars = replaceRole($chars, "Mitomane", "Villico M");

            sleep(2);
        }
}

echo "\n";

// Creo la condizione per l'utilizzo del Gufo Letale. Se il numero di giocatori e' maggiore o uguale a 20, il Gufo diventa Gufo Letale, pertanto ha il potere di uccidere direttamente un giocatore, nessuno escluso.

if ($group >= 20) 
{
    echo RED;
    echo "ATTENZIONE\n";
    echo RESET;

    echo YELLOW;
    echo "Essendo 20+ giocatori, il Gufo e' diventato Gufo Letale, pertanto non decidera' piu' chi mandare al ballottaggio, ma avra' il potere indicare qualcuno da uccidere.\n";
    echo RESET;
    
    $chars = replaceRole($chars, "Gufo", "Gufo Letale");

    sleep(2);
}
            
echo "\n";

// Creo un array vuoto per immagazzinare i personaggi morti.
$deadChars = [];

// Valorizzo i potenziali morti e il potenziale gufato come null in modo tale da non avere errori a inizio partita e poterli richiamare dal secondo ciclo in poi.
$wolfKilled = null;
$seerKilled = null;
$owlKilled = null;
$owl = null;

// Finche' false, il gioco continuera'.
$end = false;

// Utilizzo un ciclo for per generare giorni e notti per la votazione, i poteri dei personaggi, l'uccisione dei lupi e/o il salvataggio della Guardia del Corpo.
for ($i = 1; $end === false; $i++) 
{    
    // GIORNO - Votazione
    echo str_repeat("-", 158) . "\n\n";
    echo CYAN;
    echo "Giorno $i\n";
    echo RESET;

    // if ($i > 1)
    // {
    //     echo "E' giorno e ";

    //     // Mostro chi e' morto controllando tutte le condinzioni possibili.
    //     if ($group >= 20 && $wolfKilled && $seerKilled && $owlKilled)
    //     {
    //         echo BGRED . "$wolfKilled, $seerKilled e $owlKilled sono morti/e." . RESET . "\n";
    //         echo YELLOW . "Il nuovo giro iniziera' da $wolfKilled." . RESET . "\n";
    //     }
    //     elseif ($group >= 20 && $seerKilled && $owlKilled)
    //     {
    //         echo BGRED . "$seerKilled e $owlKilled sono morti/e." . RESET . "\n";
    //         echo YELLOW . "Il giro rimane invariato." . RESET . "\n";
    //     }
    //     elseif ($group >= 20 && $wolfKilled && $owlKilled)
    //     {
    //         echo BGRED . "$wolfKilled e $seerKilled sono morti/e." . RESET . "\n";
    //         echo YELLOW . "Il nuovo giro iniziera' da $wolfKilled." . RESET . "\n";
    //     }
    //     elseif ($group == 13 && $group >= 15 && $wolfKilled && $seerKilled)
    //     {
    //         echo BGRED . "$wolfKilled e $seerKilled sono morti/e." . RESET . "\n";
    //         echo YELLOW . "Il nuovo giro iniziera' da $wolfKilled." . RESET . "\n";
    //     }
    //     elseif ($group >= 20 && $owlKilled) 
    //     {
    //         echo BGRED . "$owlKilled e' morto/a." . RESET . "\n";
    //         echo YELLOW . "Il giro rimane invariato." . RESET . "\n";
    //     }
    //     elseif ($group == 13 && $group >= 15 && $seerKilled)
    //     {
    //         echo BGRED . "$seerKilled e' morto/a." . RESET . "\n";
    //         echo YELLOW . "Il giro rimane invariato." . RESET . "\n";
    //     }
    //     elseif ($wolfKilled)
    //     {
    //         echo BGRED . "$wolfKilled e' morto/a." . RESET . "\n";
    //         echo YELLOW . "Il nuovo giro iniziera' da $wolfKilled." . RESET . "\n";
    //     }
    //     else 
    //     {
    //         echo BGGREEN . "non e' morto nessuno." . RESET . "\n";
    //         echo YELLOW . "Il giro rimane invariato." . RESET . "\n";
    //     }
    // }

    echo "\n";

    sleep(1);

    // Tempo per la discussione da regolamento prima di passare alla votazione: 3 minuti.
    loader("Avete 3 minuti per discutere, si vota", 3);

    echo YELLOW;
    readline("Voto e motivazione, i personaggi morti votano senza motivazione.\n");
    echo RESET;
    echo "\n";

    // Chiedo ed elenco i voti di ogni partecipante, vivo o morto. Se morto, senza motivazione. Per ora lo faccio a mano, in futuro l'elenco sara' automatico a seconda del primo di giro.
    // for ($i = 1; $i < $group; $i++) 
    // { 
    //     readline("Il $i di giro: ");
    //     echo YELLOW;
    //     readline("vota:\n");
    //     echo RESET;
    // }

    // foreach ($players as $player)
    // {
    //     readline("Inserisci il nome del giocatore votato per il ballottaggio:\n");
    // }

    echo "\n";

    // Mostro il gufato, ovvero il primo del ballottaggio, se esiste.
    // if ($group >= 12 && $group < 20 && array_key_exists("Gufo", $chars) && $owl) 
    // {
    //     echo YELLOW;
    //     echo "Vanno al ballottaggio\n";
    //     echo RESET;

    //     $voted1 = $owl;
    //     echo "$owl\n";
    //     $voted2 = readline("\n");

    //     echo "\n";

    //     echo "$voted1 inizia a discolparsi.\n";
        
    //     sleep(2);
    // }
    // else 
    // {
    //     echo YELLOW;
    //     echo "Vanno al ballottaggio\n";
    //     echo RESET;

    //     $voted1 = readline("Inserisci il nome del giocatore piu' votato:\n");
    //     $voted2 = readline("Inserisci il nome del secondo giocatore piu' votato:\n");
        
    //     echo "\n";

    //     echo "$voted1 inizia a discolparsi.\n";

    //     sleep(2);
    // }

    // echo "\n";
    
    // readline("Premi ENTER quando $voted1 e $voted2 finiscono di discolparsi per procedere all'eliminazione.\n");
    
    // echo "\n";
    
    // echo RED;
    // readline("Votazione secca, solo per personaggi vivi, senza motivazione!\n");
    // echo RESET;
    // echo "\n";

    // echo "\n";

    // for ($i = 1; $i < (count($chars) - 1); $i++) 
    // { 
    //     readline("Il $i vivo di giro:\n");
    //     echo YELLOW;
    //     readline("vota:\n");
    //     echo RESET;
    // }

    // for ($c = 0; $c < (count($chars) - 2) ; $c++)
    // { 
    //     readline("Inserisci il nome del giocatore votato per l'eliminazione:\n");
    // }

    // echo "\n";

    $voted = readline("Il consiglio vota per l'eliminazione:\n");
    $votedRole = array_search($voted, $chars);
    $chars = kill($chars, $voted);

    echo "\n";
    echo BGRED . "$voted e' morto/a!" . RESET . "\n";
    echo "\n";

    sleep(1);

    echo "\n";
    
    // Un personaggio morto perde tutti i suoi poteri, ma per essere esaminato dal Medium ho bisogno di conoscere il ruolo in ogni momento, pertanto non potro' semplicemente usare array_push() per popolare l'array dei morti ma dovro' catturare il ruolo prima che venga eliminato dall'array finale e ricreare l'array ruolo => nome anche per i personaggi morti.
    $deadChars[$votedRole] = $voted;
    
    // Stampo gli array fantasmi/vivi con counter.
    echo BGWHITE . "FANTASMI (". count($deadChars) . ")" . RESET . "\n";
    print_r($deadChars);
    
    echo "\n\n";
    
    echo GREEN . "VIVI (". count($chars) . ")" . RESET . "\n";
    print_r($chars);

    echo "\n";

    // Catturo le chiavi dei Lupi in modo tale da controllare se vivi o meno per determinare la fine della partita.
    $wolf1 = array_key_exists("Lupo Mannaro 1", $chars);
    $wolf2 = array_key_exists("Lupo Mannaro 2", $chars);
    $wolf3 = array_key_exists("Lupo Mannaro 3", $chars);
    $wolfM = array_key_exists("Lupo Mannaro M", $chars);

    // Controllo lo stato dei Lupi, se almeno 1 e' vivo il gioco continua, altrimenti hanno vinto i buoni o il Criceto Mannaro se ancora in vita (da fare). Se si esci dal ciclo (break) e termina la partita mostrando i vincitori.
    if ($wolf1 || $wolf2 || $wolf3) 
    {
        $end = false;
    }
    else 
    {
        $end = true;
        
        echo str_repeat("-", 44) . "\n";
        echo "|" . BGGREEN . " Il gioco e' finito, hanno vinto I BUONI! " . RESET . "|\n";
        echo str_repeat("-", 44) . "\n";
        
        echo "\n";

        // Prima di creare la condizione per la vittoria del Criceto Mannaro faccio un controllo preliminare in questa condizione. In caso di vittoria dei buoni, se il numero iniziale dei giocatori inserito non e' pari a 13 ne' maggiore o uguale a 15, il Criceto non puo' esistere, pertanto in caso di vittoria dei buoni, esci tranquillamente dal ciclo e mostra i buoni come vincitori.
        if ($end && ($group == 13 || $group >= 15) && array_key_exists("Criceto Mannaro", $chars)) 
        {
            sleep(1);

            echo "\n";

            echo str_repeat("-", 45) . "\n";
            echo "|" . BGMAGENTA . " E INVECE NO, HA VINTO IL CRICETO MANNARO! " . RESET . "|\n";
            echo str_repeat("-", 45) . "\n";

            echo "\n";
            
            break;
        }

        break;
    }

    // Creo le condizioni per la vittoria del lupi, che vincono se il numero dei buoni ancora in vita e' uguale al numero dei lupi ancora in vita. Se si, esci dal ciclo (break) e termina la partita mostrando i vincitori.
    if (count($chars) == 6 && $wolf1 && $wolf2 && $wolf3 && $wolfM) 
    {
        $end = true;
        
        echo str_repeat("-", 43) . "\n";
        echo "|" . BGRED . " Il gioco e' finito, hanno vinto I LUPI! " . RESET . "|\n";
        echo str_repeat("-", 43) . "\n";

        echo "\n";
        
        break;

    }
    elseif (count($chars) == 4 && (
        $wolf1 && $wolf2 ||
        $wolf1 && $wolf3 ||
        $wolf1 && $wolfM ||
        $wolf2 && $wolf3 ||
        $wolf2 && $wolfM ||
        $wolf3 && $wolfM
        )) 
    {  
        $end = true;
        
        echo str_repeat("-", 43) . "\n";
        echo "|" . BGRED . " Il gioco e' finito, hanno vinto I LUPI! " . RESET . "|\n";
        echo str_repeat("-", 43) . "\n";

        echo "\n";
        
        break;
    }
    elseif (count($chars) == 2 && ($wolf1 || $wolf2 || $wolf3 || $wolfM)) 
    {
        $end = true;
     
        echo str_repeat("-", 43) . "\n";
        echo "|" . BGRED . " Il gioco e' finito, hanno vinto I LUPI! " . RESET . "|\n";
        echo str_repeat("-", 43) . "\n";

        echo "\n";
        
        break;
    }

    // Creo la condizione per la vittoria del Criceto Mannaro, che vince se riesce a rimanere in vita fino alla fine della partita.
    if ($end && ($group == 13 || $group >= 15) && array_key_exists("Criceto Mannaro", $chars))
    {
        sleep(1);

        echo "\n";

        echo str_repeat("-", 45) . "\n";
        echo "|" . BGMAGENTA . " E INVECE NO, HA VINTO IL CRICETO MANNARO! " . RESET . "|\n";
        echo str_repeat("-", 45) . "\n";

        echo "\n";
        
        break;
    }

    
    // NOTTE - Poteri dei personaggi.
    echo str_repeat("-", 158) . "\n\n";
    echo BLUE;
    echo "Notte $i\n";
    echo RESET;
    echo "E' notte e tutti chiudano gli occhi.\n";
    echo "\n";
    

    // Veggente
    // Se ancora vivo, il Veggente potra' utilizzare il suo potere.
    if (array_key_exists("Veggente", $chars)) 
    {
        $seer = readline("Il Veggente indichi un giocatore tra i vivi da esaminare:\n");
        $seerRole = array_search($seer, $chars);
        
        // Prima regola del Criceto Mannaro: Se esiste e viene esaminato dal veggente, muore.
        if (($group == 13 || $group >= 15) && $seerRole === "Criceto Mannaro") 
        {   
            $seerKilled = $seer;
            $seerKilledRole = $seerRole;

            $chars = kill($chars, $seerKilled);

            echo "\n";
            echo BGRED . "$seer e' buono/a, ma e' morto/a!" . RESET . "\n";
            echo "\n";

            sleep(1);
            
            $deadChars[$seerKilledRole] = $seerKilled;
        }
        elseif (
            $seerRole === "Lupo Mannaro 1" || 
            $seerRole === "Lupo Mannaro 2" || 
            $seerRole === "Lupo Mannaro 3" || 
            $seerRole === "Lupo Mannaro M"
            ) 
        {
            echo RED . "$seer e' un Lupo!" . RESET . "\n";
            echo "\n";

            sleep(1);
        }
        else 
        {
            echo CYAN . "$seer e' buono/a." . RESET . "\n";
            echo "\n";

            sleep(1);
        }
    }
    

    // Medium
    // Se il numero di giocatori e' maggiore o uguale a 9 e se ancora vivo, il Medium potra' utilizzare il suo potere.
    if ($group >= 9 && array_key_exists("Medium", $chars)) 
    {
        $medium = readline("Il Medium indichi un giocatore tra i morti da esaminare:\n");
        $mediumRole = array_search($medium, $deadChars);

        if (
            $mediumRole === "Lupo Mannaro 1" || 
            $mediumRole === "Lupo Mannaro 2" || 
            $mediumRole === "Lupo Mannaro 3" || 
            $mediumRole === "Lupo Mannaro M") 
        {  
            echo RED . "$medium era un Lupo!" . RESET . "\n";
            echo "\n";

            sleep(1);
        }
        else 
        {
            echo CYAN . "$medium era buono/a." . RESET . "\n";
            echo "\n";

            sleep(1);
        }
    }


    // Lupi
    $wolfKilled = readline("I Lupi indichino un giocatore da uccidere:\n");
    $wolfKilledRole = array_search($wolfKilled, $chars);

    // Guardia del Corpo
    // Se il numero di giocatori e' maggiore o uguale a 11, la Guardia del corpo sara' presente e potra' utilizzare il suo potere.
    if ($group >= 11 && array_key_exists("Guardia del Corpo", $chars)) 
    {
        $protected = readline("La Guardia del Corpo indichi un giocatore da proteggere:\n");
    }
    else 
    {
        $protected = null;
    }
    
    // Se il personaggio scelto dai Lupi non e' lo stesso protetto dalla Guardia del Corpo, riempio l'array dei morti come gia' visto per il personaggio votato.
    if ($wolfKilled !== $protected && $wolfKilledRole !== "Criceto Mannaro") 
    {
        $chars = kill($chars, $wolfKilled);

        echo "\n";
        echo BGRED . "$wolfKilled e' morto/a!" . RESET . "\n";
        echo "\n";

        sleep(1);

        $deadChars[$wolfKilledRole] = $wolfKilled;

        echo YELLOW;
        echo "Il nuovo giro iniziera' da $wolfKilled.\n";
        echo RESET;

        sleep(1);

        echo "\n";
    }
    else 
    {
        echo "\n";
        echo BGGREEN . "Non e' morto nessuno." . RESET . "\n";
        echo "\n";

        sleep(1);

        echo YELLOW;
        echo "Il giro rimane invariato.\n";
        echo RESET;

        sleep(1);

        echo "\n";
    }


    // Gufo
    // Se il numero di giocatori e' maggiore o uguale a 12 e se ancora vivo, il Gufo potra' utilizzare il suo potere.
    if ($group >= 12 && array_key_exists("Gufo", $chars))
    {
        $owl = readline("Il Gufo indichi un giocatore da mandare direttamente al ballottaggio:\n");
        $owlRole = array_search($owl, $chars);
    }
    elseif ($group >= 20 && array_key_exists("Gufo Letale", $chars))
    {
        //Gufo Letale, non manda al ballottaggio, ma uccide.
        $owlKilled = readline("Il Gufo Letale indichi un giocatore da uccidere:\n");
        $owlKilledRole = array_search($owlKilled, $chars);

        $chars = kill($chars, $owlKilled);

        echo "\n";
        echo BGRED . "$owlKilled e' morto/a!" . RESET . "\n";
        echo "\n";

        sleep(1);

        $deadChars[$owlKilledRole] = $owlKilled;
    }


    echo "\n";
    
    echo BGWHITE . "FANTASMI (". count($deadChars) . ")" . RESET . "\n";
    print_r($deadChars);
    
    echo "\n";
    
    echo GREEN . "VIVI (". count($chars) . ")" . RESET . "\n";
    print_r($chars);

    echo "\n";


    // Controllo ancora se ci sono le condizioni per la vittoria dei lupi.
    if (count($chars) == 6 && $wolf1 && $wolf2 && $wolf3 && $wolfM) 
    {  
        $end = true;
        
        echo str_repeat("-", 43) . "\n";
        echo "|" . BGRED . " Il gioco e' finito, hanno vinto I LUPI! " . RESET . "|\n";
        echo str_repeat("-", 43) . "\n";

        echo "\n";
    }
    elseif (count($chars) == 4 && (
        $wolf1 && $wolf2 ||
        $wolf1 && $wolf3 ||
        $wolf1 && $wolfM ||
        $wolf2 && $wolf3 ||
        $wolf2 && $wolfM ||
        $wolf3 && $wolfM
        )) 
    {    
        $end = true;
        
        echo str_repeat("-", 43) . "\n";
        echo "|" . BGRED . " Il gioco e' finito, hanno vinto I LUPI! " . RESET . "|\n";
        echo str_repeat("-", 43) . "\n";

        echo "\n";     
    }
    elseif (count($chars) == 2 && ($wolf1 || $wolf2 || $wolf3 || $wolfM)) 
    { 
        $end = true;
     
        echo str_repeat("-", 43) . "\n";
        echo "|" . BGRED . " Il gioco e' finito, hanno vinto I LUPI! " . RESET . "|\n";
        echo str_repeat("-", 43) . "\n";

        echo "\n";
    }

    // Controllo ancora se si verifica la condizione per la vittoria del Criceto Mannaro.
    if ($end && ($group == 13 || $group >= 15) && array_key_exists("Criceto Mannaro", $chars)) 
    {
        sleep(1);

        echo "\n";

        echo str_repeat("-", 45) . "\n";
        echo "|" . BGMAGENTA . " E INVECE NO, HA VINTO IL CRICETO MANNARO! " . RESET . "|\n";
        echo str_repeat("-", 45) . "\n";

        echo "\n";
    }
}


    
    