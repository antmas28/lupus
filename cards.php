<?php

echo "\n";
echo GREEN;
echo "Bene, passiamo all'assegnazione dei ruoli!\n";
echo RESET;
echo "\n";

// Creo un array nel quale inserire i ruoli da assegnare in ordine, a seconda del numero dei giocatori
$usedRoles = [];

for ($i = 1; $i <= $group; $i++) 
{
    $usedRoles[$i - 1] = $roles[$i - 1];
}

// Massone 1 e Massone 2, indici 12 e 13, devono per forza giocare in coppia, pertanto se il numero di giocatori e' pari a 13, il Massone 1 verra' automaticamente sostituito dall'indice 14 dell'array $roles dei personaggi (Criceto Mannaro), ovvero il primo personaggio utile successivo
if ($group == 13) 
{
    $usedRoles[12] = $roles[14];
    array_values($usedRoles);
}

// Randomizzo l'array creato con i ruoli da assegnare
shuffle($usedRoles);

// Creo un array per i giocatori inseriti dall'utente che si combineranno con ruoli presenti nell'array dei ruoli randomizzato
$players = [];

for ($i = 1; $i <= $group; $i++) 
{
    $players[$i - 1] = readline("Inserisci il nome del Giocatore $i: ");
}

// Combino gli array ruoli random e giocatori inseriti formando finalmente l'array dei personaggi giocanti
$chars = array_combine($usedRoles, $players);

echo "\n";
loader("Mescolo il mazzo e consegno le carte", 3);

echo "\n";
echo GREEN;
echo "Ecco i personaggi iniziali!\n";
echo RESET;
echo "\n";
print_r($chars);

// Scelgo randomicamente il primo di giro sfruttando il numero di giocatori inseriti dall'utente come limite
echo "\n";
$start = $players[rand(0, $group - 1)];

sleep(2);

echo YELLOW;
echo "Il giocatore che dara' inizio al giro e' $start.\n";
echo RESET;

sleep(2);

// Fine assegnazione
echo "\n";
echo GREEN;
echo "Sei pronto per moderare!\n";
echo RESET;
echo "\n";
